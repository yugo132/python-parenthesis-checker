class Ppc(object):
    """Main Class Python Parenthesis Checker"""
    def __init__(self):
        super(Ppc, self).__init__()

    def check(self, string):
        if(type(string) == str):
            parenthesis = list()
            event = False
            for char in string:
                if(char == "("):
                    event = True
                    parenthesis.append(char)

                elif(char == ")" and not len(parenthesis)):
                    return False

                elif(char == ")"):
                    parenthesis.pop()

            if not len(parenthesis) and event:
                return True

        return False