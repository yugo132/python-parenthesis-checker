<p>This is a single class to check parenthesis syntax on string.</p>
<h1>How to use?</h1>
<p>just import and create a instance of ppc</p>
<pre>
    <code>
        from ppc import Ppc

        checker = new Ppc()
        checker.check("(x+344343(43*34))")
    </code>
</pre>