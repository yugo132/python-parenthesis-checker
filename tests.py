import unittest
from ppc import Ppc

class PpcTestCase(unittest.TestCase):
    """Unit Testing for Ppc Class"""
    def setUp(self):
        self.ppc = Ppc()

    def tearDown(self):
        del self.ppc

    def test_check_syntax_parenthesis_false(self):
        result = self.ppc.check("(())))())(")
        self.assertEqual(result,False)

    def test_check_syntax_parenthesis_true(self):
        result = self.ppc.check("(((())()()))")
        self.assertEqual(result,True)

    def test_check_params_other_than_string(self):
        result = self.ppc.check(4345)
        self.assertEqual(result,False)


    def test_check_params_string_without_parenthesis(self):
        result = self.ppc.check("4345dfdfasdf")
        self.assertEqual(result,False)

if __name__ == "__main__":
    unittest.main()
